const express = require('express');
const path = require('path');
const logger = require('morgan');
const history = require('connect-history-api-fallback');

const app = express();

app.use(history());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'dist')));

module.exports = app;
